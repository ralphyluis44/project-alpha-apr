from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


@login_required
def create_task_view(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/task.html", context)


@login_required
def my_tasks_view(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/my_tasks.html", {"tasks": tasks})
