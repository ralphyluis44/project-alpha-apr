from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


@login_required
def list_view(request):
    list = Project.objects.filter(owner=request.user)
    context = {
        "list_view": list,
    }
    return render(request, "projects/list.html", context)


@login_required
def detail_view(request, id):
    project = get_object_or_404(Project, pk=id)
    context = {"project": project}
    return render(request, "projects/detail.html", context)


@login_required
def create_view(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
